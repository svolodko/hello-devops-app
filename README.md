# hello-devops-app

A simple Create React App (CRA) to use when practicing DevOps.

## Step 1: Run the app locally

```bash
brew install nvm
# ... configure nvm
nvm install
nvm use
yarn install
yarn test
yarn start
```

## Step 2: Deploy with Heroku

```bash
brew install heroku
heroku login
heroku create
git push heroku master
heroku apps:open
```

![](img/deploy-heroku.png)

## Deploy via Gitlab CI/CD pipeline

![](img/deploy-gitlab.png)

```yaml
# contents of .gitlab-ci.yml
stages:
  - test
  - build
  - deploy

test:
  stage: test
  image: node:12.15.0
  script:
    - yarn install
    - yarn test

build:
  stage: build
  image: node:12.15.0
  script:
    - yarn install
    - yarn build
  artifacts:
    paths:
      - build/

deploy:
  stage: deploy
  image: alpine
  variables:
    # AWS_ACCESS_KEY_ID: XXX
    # AWS_SECRET_ACCESS_KEY: XXX
    MOCK_DEPLOY_ENDPOINT: https://xztikcpzd9.execute-api.us-west-2.amazonaws.com/demo/deploy
  script:
    # - aws s3 cp --sync build s3://my-bucket
    - apk add curl
    - curl -s ${MOCK_DEPLOY_ENDPOINT}?name=${GITLAB_USER_LOGIN}
  needs:
    - job: build
      artifacts: true
  when: manual
```

## Takeaways

* Develop an application with an eye toward deployment.
* Move deployments off of developer machines.
* Invest the time to learn a CI/CD tool (GitLab, Travis, Circle CI, GitHub Actions)